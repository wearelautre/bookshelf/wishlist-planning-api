package bzh.lautre.bookshelfwishlistplanningapi

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class BookshelfWishlistPlanningApiApplication

fun main(args: Array<String>) {
	runApplication<BookshelfWishlistPlanningApiApplication>(*args)
}
