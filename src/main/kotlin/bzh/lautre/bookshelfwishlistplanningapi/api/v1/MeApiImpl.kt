package bzh.lautre.bookshelfwishlistplanningapi.api.v1

import bzh.lautre.bookshelf.wishlistplanning.api.v1.MeApi
import bzh.lautre.bookshelf.wishlistplanning.api.v1.model.UserDTO
import bzh.lautre.bookshelfwishlistplanningapi.UserDetails
import org.springframework.http.ResponseEntity
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.bind.annotation.RestController


@RestController
class MeApiImpl: MeApi {


    override fun getMe(
    ): ResponseEntity<UserDTO> {
        val userDetails: UserDetails = SecurityContextHolder.getContext().authentication.principal as UserDetails
        return ResponseEntity.ok(UserDTO().email(userDetails.id))
    }

}