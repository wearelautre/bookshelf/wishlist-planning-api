package bzh.lautre.bookshelfwishlistplanningapi

data class UserDetails(
    val id: String,
    val email: String,
    val name: String
)