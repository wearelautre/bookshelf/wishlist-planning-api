package bzh.lautre.bookshelfwishlistplanningapi.config.security

import bzh.lautre.bookshelfwishlistplanningapi.UserDetails
import org.springframework.security.authentication.AbstractAuthenticationToken
import org.springframework.security.core.GrantedAuthority

class CustomAuthenticationToken(
    private val name: String,
    private val id: String,
    private val email: String,
    authorities: MutableCollection<out GrantedAuthority> = mutableSetOf()
) : AbstractAuthenticationToken(authorities) {
    override fun isAuthenticated(): Boolean  = true
    override fun getCredentials(): String  = this.email
    override fun getPrincipal(): UserDetails = UserDetails(id, email, name)
}
